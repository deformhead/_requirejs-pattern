define(

    function module( ) {

        'use strict' ;

        var Pattern ;

        Pattern = function Pattern( map ) {

            var animations ;
            var current ;
            var elapsed ;
            var frame ;
            var initialize ;

            Pattern.prototype.render = function render( render ) {

                // call user's render callback
                render( animations[ current ][ frame ] ) ;
            } ;

            Pattern.prototype.update = function update( delta ) {

                // if animation has change then start the new animation
                if ( this.next !== null ) {

                    // define the new animation
                    current = this.next ;

                    elapsed = 0 ;
                    frame = 0 ;

                    this.next = null ;
                }

                // if animation didn't change and if framerate is greater than 0 then update values
                else if ( this.framerate > 0 ) {

                    // define the elapsed time since the beginning of current animation
                    elapsed = ( elapsed + delta ) % ( animations[ current ].length * 1000 / this.framerate ) ;

                    // define the current animation frame
                    frame = Math.floor( elapsed * this.framerate / 1000 ) % animations[ current ].length ;
                }
            } ;

            initialize = function initialize( map ) {

                this.framerate = 0 ;
                this.next = null ;

                animations = map ;
                current = 'idle' ;
                elapsed = 0 ;
                frame = 0 ;

            // keep the context
            }.bind( this ) ;

            // initialize this instance
            initialize( map ) ;
        } ;

        return ( Pattern ) ;
    }
) ;
